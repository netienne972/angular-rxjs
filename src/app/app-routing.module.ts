import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {SecondPageComponent} from "./second-page/second-page.component";
import {FirstPageComponent} from "./first-page/first-page.component";


const routes: Routes = [
  {path: 'second', component: SecondPageComponent,},
  {path: '**', component: FirstPageComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
