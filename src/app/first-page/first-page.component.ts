import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval, map, Observable, Subject, take, tap} from "rxjs";

@Component({
  selector: 'app-first-page',
  templateUrl: './first-page.component.html',
  styleUrls: ['./first-page.component.css']
})
export class FirstPageComponent implements OnInit, OnDestroy {

  public observablePipeAsync$ ?: Observable<String>;
  public observableManual$ ?: Observable<String>;

  ngOnInit(): void {
    this.observablePipeAsync$ = interval(1_000 /* ms */).pipe(
      tap(v => console.log('[Pipe] Receive a value : ' + v)),
      map((i) => `${++i}`),
      take(20),
    );

    this.observableManual$ = new Subject<String>();
    interval(1_000 /* ms */).pipe(
      tap(v => console.log('[Manual] Receive a value : ' + v)),
      map((i) => `${++i}`),
      take(20),
    ).subscribe( /* DO something with the event */);
  }

  ngOnDestroy(): void {

  }
}
